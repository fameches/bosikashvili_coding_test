import React from 'react';
import { connect } from 'react-redux';
import { findClosestWarehouse } from '../../actions';

import logo from '../../assets/images/logo.png';
import styles from './styles.css';

class ShippingEstimate extends React.Component {
  state = {
    zipCode: {
      value: '',
      valid: false,
      validMsg: '',
      pristine: false,
    },
  };

  handleSubmit = (event) => {
    const { zipCode } = this.state;

    event.preventDefault();
    this.props.findClosestWarehouse(zipCode.value);
  };

  handleZipCodeChange = (event) => {
    if (event.target.value.length <= 5) {
      this.setState({
        zipCode: {
          ...this.state.zipCode,
          value: event.target.value,
        }
      }, this.validate);
    }
  };

  handleZipCodeBlur = () => {
    const { pristine } = this.state.zipCode;
  
    if (!pristine) {
      this.setState({
        zipCode: {
          ...this.state.zipCode,
          pristine: true,
        }
      });
    }
  };

  validate() {
    const { zipCode } = this.state;
    let valid = true;
    let validMsg = '';

    if (!zipCode.value) {
      valid = false;
    } else if (!/^[0-9]{5}$/.test(zipCode.value)) {
      valid = false;
      validMsg = 'The US zip code must contain 5 digits.';
    }

    this.setState({
      zipCode: {
        ...this.state.zipCode,
        valid,
        validMsg,
      }
    });
  }

  getTimeEstimation() {
    const { distance } = this.props.route;
    if (distance > 2000) {
      return '4+ days';
    } else if (distance > 600) {
      return '3-4 days';
    } else if (distance > 100) {
      return '2 days';
    } else {
      return '1 day';
    }
  }

  render() {
    const { zipCode } = this.state;
    const { route } = this.props;
    const errorMsg = zipCode.validMsg || (zipCode.value === route.zipCode && route.errorMsg);

    return (
      <div className={styles.shippingEstimate}>
        <img className={styles.logo} src={logo} />
        <form className={styles.form} onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Enter zip code"
            className={styles.field}
            value={zipCode.value}
            onChange={this.handleZipCodeChange}
            onBlur={this.handleZipCodeBlur}
          />
          {zipCode.pristine && errorMsg &&
            <div className={styles.errorMsg}>
              {zipCode.pristine && errorMsg}
            </div>
          }
          <button
            className={styles.submit}
            disabled={!zipCode.valid || route.loading || errorMsg}
          >
            {route.loading ? '...loading' : 'Calculate'}
          </button>
        </form>
        {route.from && route.to &&
          <div className={styles.route}>
            <div className={styles.point}>
              <h2 className={styles.title}>Warehouse</h2>
              <div className={styles.zipCode}>{route.from.zip_code}</div>
              <div className={styles.location}>{`${route.from.city}, ${route.from.state}`}</div>
            </div>
            <div className={styles.result}>
              <div className={styles.estimation}>It takes {this.getTimeEstimation()}</div>
              <div className={styles.line}></div>
              <div>{route.distance} miles</div>
            </div>
            <div className={styles.point}>
              <h2 className={styles.title}>Your location</h2>
              <div className={styles.zipCode}>{route.to.zip_code}</div>
              <div className={styles.location}>{`${route.to.city}, ${route.to.state}`}</div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  route: state.route,
});

const mapDispatchToProps = {
  findClosestWarehouse,
};

export default connect(mapStateToProps, mapDispatchToProps)(ShippingEstimate);
