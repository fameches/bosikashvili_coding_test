import React from 'react';
import { Provider } from 'react-redux';
import ShippingEstimate from './ShippingEstimate';

import store from '../store';

const App = (props) => {
  return (
    <Provider store={store}>
      <ShippingEstimate />
    </Provider>
  )
};

export default App;
