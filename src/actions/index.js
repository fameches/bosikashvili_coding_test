import axios from 'axios';

import {
  WAREHOUSE_ZIP_CODES,
  GET_DISTANCE_URL,
  FIND_CLOSEST_WAREHOUSE_REQUEST,
  FIND_CLOSEST_WAREHOUSE_SUCCESS,
  FIND_CLOSEST_WAREHOUSE_FAIL,
} from "../constants";


export const findClosestWarehouse = (zipCode) => dispatch => {
  const promises = [];

  dispatch({
    type: FIND_CLOSEST_WAREHOUSE_REQUEST,
    payload: {
      zipCode,
    }
  });

  for (const warehouseZipCode of WAREHOUSE_ZIP_CODES) {
    const getRoute = axios.get(`${GET_DISTANCE_URL}/${warehouseZipCode}/${zipCode}`)
      .then(res => {
        if (!res.data[zipCode]) throw new Error('Zip code is incorrect');
        if (res.data[zipCode].error_msg) throw new Error(res.data[zipCode].error_msg);

        return {
          from: res.data[warehouseZipCode],
          to: res.data[zipCode],
          distance: res.data[`${warehouseZipCode}-${zipCode}`].distance,
        };
      });

    promises.push(getRoute);
  }

  axios.all(promises)
    .then(result => {
      const closest = result.reduce((closest, current) =>
        closest.distance > current.distance ? current : closest
      );

      closest.zipCode = zipCode;

      dispatch({
        type: FIND_CLOSEST_WAREHOUSE_SUCCESS,
        payload: closest,
      });
    })
    .catch((error) => {
      dispatch({
        type: FIND_CLOSEST_WAREHOUSE_FAIL,
        payload: {
          errorMsg: error.message,
          zipCode,
        },
      });
    });
}
