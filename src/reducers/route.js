import {
  FIND_CLOSEST_WAREHOUSE_REQUEST,
  FIND_CLOSEST_WAREHOUSE_SUCCESS,
  FIND_CLOSEST_WAREHOUSE_FAIL,
} from "../constants";

const initState = {
  from: null,
  to: null,
  zipCode: null,
  distance: 0,
  loading: false,
  errorMsg: '',
}

export default function route(state = initState, action) {
  switch (action.type) {
    case FIND_CLOSEST_WAREHOUSE_REQUEST:
      return {
        ...initState,
        zipCode: action.payload.zipCode,
        loading: true,
      };
    case FIND_CLOSEST_WAREHOUSE_SUCCESS:
      return {
        ...action.payload,
        errorMsg: '',
        loading: false,
      };
    case FIND_CLOSEST_WAREHOUSE_FAIL:
      return {
        ...initState,
        errorMsg: action.payload.errorMsg,
        zipCode: action.payload.zipCode,
        loading: false,
      };
    default:
      return state;
  }
}
